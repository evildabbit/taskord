@extends('layouts.app')

@section('content')
<div class="container">
    @include('user.profile')
    <div class="row justify-content-center mt-4">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    @if (Auth::check() && Auth::user()->id === $user->id)
                        @livewire('create-task')
                    @endif
                    @livewire('user.products', [
                        'user' => $user,
                        'page' => 1,
                        'perPage' => 1
                    ])
                </div>
                @include('user.sidebar')
            </div>
        </div>
    </div>
</div>
@endsection
